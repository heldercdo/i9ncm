﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace i9NCM.Engine.Controller
{
    class Tecwin
    {
        public CookieContainer cookies;
        public string _loginAddress = "http://tecwinweb.aduaneiras.com.br/Handlers/Modulos/Usuario/Login.ashx";

        public Tecwin(string user, string password)
        {
            cookies = new CookieContainer();
            this.HttpPost(_loginAddress,
                            "action=2"
                            + "&login=" + user
                            + "&senha=" + password
                            + "&salvarLogin=false");
        }

        public string HttpPost(string url, string postData)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
                System.Net.ServicePointManager.Expect100Continue = false;
                request.CookieContainer = this.cookies;
                request.Method = "POST";
                request.UserAgent = "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0; SLCC2; Media Center PC 6.0; InfoPath.3; MS-RTC LM 8; Zune 4.7)";
                byte[] bufferPostData = Encoding.UTF8.GetBytes(postData);
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = bufferPostData.Length;
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(bufferPostData, 0, bufferPostData.Length);
                WebResponse response = request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8, true);
                string output = reader.ReadToEnd();
                dataStream.Close();
                reader.Close();
                response.Close();
                this.cookies = request.CookieContainer;
                return output;
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
