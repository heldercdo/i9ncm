﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace i9NCM.Engine.Controller
{
    public class Importacao
    {
        Model.IMPORTACAO _importacao;
        Tecwin _tecwin;

        public Importacao()
        {
            //_importacao = new Model.IMPORTACAO();
            //_importacao.dt_inicio = DateTime.Now;
        }

        public void Importar()
        {
            using (var conexao = new Model.i9NCMEntities())
            {
                var result = (from impo in conexao.IMPORTACAO
                              select impo).OrderByDescending(i => i.cd_importacao).ToList();
                if (result.Count == 0 || result[0].dt_fim != null)
                {
                    Console.WriteLine(DateTime.Now + " => Nova importacao");
                    _importacao = new Model.IMPORTACAO();
                    _importacao.dt_inicio = DateTime.Now;

                    conexao.IMPORTACAO.Add(_importacao);
                    conexao.SaveChanges();
                }
                else
                {
                    _importacao = result[0];
                    Console.WriteLine(DateTime.Now + " => Continuacao da importacao " + _importacao.cd_importacao);
                }

                var arrCdNCM = conexao.SP_RETORNAR_NCM_PENDENTE(_importacao.cd_importacao).ToArray();
                int qtdeNCM = arrCdNCM.Length;
                Console.WriteLine(DateTime.Now + " => NCMs pendentes " + qtdeNCM);
                NCM[] ncmProcessadas = new NCM[qtdeNCM];
                bool booErro = false;
                try
                {
                    Console.WriteLine(DateTime.Now + " => Iniciando ciclo ");
                    Parallel.For(0, qtdeNCM,
                                index =>
                                {
                                    NCM ncm = new NCM(_importacao.cd_importacao, arrCdNCM[index], _tecwin);
                                    ncm.LoadNCM();
                                    ncm.LoadAcordos();
                                    ncmProcessadas[index] = ncm;
                                });
                    Console.WriteLine(DateTime.Now + " => Finalizando ciclo ");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(DateTime.Now + " => Erro");
                    booErro = true;
                    throw new Exception("Erro no ciclo. Importacao " + _importacao.cd_importacao, ex);
                }
                finally
                {
                    try
                    {
                        Console.WriteLine(DateTime.Now + " => Adicionando NCMs");
                        foreach (NCM ncm in ncmProcessadas)
                        {
                            if (ncm != null && ncm._ncm.cd_ncm != null && ncm._ncm.cd_ncm.Trim().Length > 0)
                                _importacao.NCM.Add(ncm._ncm);
                        }

                        Console.WriteLine(DateTime.Now + " => Salvar mudancas");
                        if (!booErro)
                            _importacao.dt_fim = DateTime.Now;
                        conexao.SaveChanges();
                        Console.WriteLine(DateTime.Now + " => Ciclo salvo");
                        conexao.SP_REGISTRAR_IMPORTACAO(_importacao.cd_importacao);
                        Console.WriteLine(DateTime.Now + " => Integracao realizada");
                    }
                    catch(Exception ex) { throw new Exception("Erro ao encerrar ciclo. " + ncmProcessadas.Length + " NCMs no buffer, " + _importacao.NCM.Count + " NCMs salvas", ex); }
                }
            }
        }

        public void AutenticarTecwin(string user, string password)
        {
            _tecwin = new Tecwin(user, password);
        }

        public void NotificarErro(String msgErro)
        {
            using (Model.LANCAMENTOEntities conexaoLancamentos = new Model.LANCAMENTOEntities())
            {
                Model.NOTIFICACAO_SERVICO notificacao = new Model.NOTIFICACAO_SERVICO();
                notificacao.cd_remetente = 1;
                notificacao.ds_assunto = "i9Crawler - Erro";
                notificacao.ds_conteudo_email = msgErro;
                notificacao.ds_destinatario = "helder.oliveira@pinho.com.br";
                notificacao.dt_registro = DateTime.Now;
                notificacao.st_html = 0;
                notificacao.st_possui_anexo = 0;
                conexaoLancamentos.NOTIFICACAO_SERVICO.Add(notificacao);
                conexaoLancamentos.SaveChangesAsync();
            }
        }
    }
}
