//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace i9NCM.Engine.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class NVE
    {
        public string cd_ncm { get; set; }
        public int cd_importacao { get; set; }
        public string nivel_nve { get; set; }
        public string atributo_nve { get; set; }
        public string especificacao_nve { get; set; }
    
        public virtual NCM NCM { get; set; }
    }
}
