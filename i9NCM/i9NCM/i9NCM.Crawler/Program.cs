﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace i9NCM.Crawler
{
    class Program
    {
        static void Main(string[] args)
        {
            bool booCicloFinalizado = false;
            while (!booCicloFinalizado)
            {
                i9NCM.Engine.Controller.Importacao impo = new Engine.Controller.Importacao();
                try
                {
                    impo.AutenticarTecwin("tec1%40pinho.com.br", "w3bt3c12");
                    impo.Importar();
                    booCicloFinalizado = true;
                }
                catch (Exception ex)
                {
                    booCicloFinalizado = true;
                    String msgErro = "";
                    while (ex != null)
                    {
                        msgErro += " ---Msg: " + ex.Message + " ||| Stack: " + ex.StackTrace;
                        if (ex is DbEntityValidationException)
                        {
                            var entityEx = (DbEntityValidationException)ex;
                            foreach (DbEntityValidationResult validation in entityEx.EntityValidationErrors)
                            {
                                foreach (DbValidationError validationError in validation.ValidationErrors)
                                    msgErro += " ||| Property name: " + validationError.PropertyName + " ||| Invalid: " + validationError.ErrorMessage;
                            }
                        }
                        if (ex.Message.ToUpper().Contains("THE OPERATION HAS TIMED OUT")
                            || ex.Message.ToUpper().Contains("O TEMPO LIMITE DA OPERAÇAO FOI ATINGIDO"))
                        {
                            booCicloFinalizado = false;
                            Console.WriteLine(DateTime.Now + " => Erro de timeout. Inicar novo ciclo");
                        }
                        ex = ex.InnerException;
                    }
                    if (booCicloFinalizado)
                        impo.NotificarErro(msgErro);
                }
            }
        }

        static string[] getNCM()
        {
            List<string> lstNcm = new List<string>();
            using (StreamReader sr = new StreamReader(Application.StartupPath + "\\" + ConfigurationManager.AppSettings["ArquivoNCM"]))
            {
                while (!sr.EndOfStream)
                    lstNcm.Add(sr.ReadLine());
            }
            return lstNcm.ToArray();
        }
    }
}
